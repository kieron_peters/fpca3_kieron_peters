module BlackJack
(drawDealerCards
    ,startPlayerCards
    ,drawPlayerCards
    ,handValue
    ,showCards
    ,handDisplay
    ,displayCard
    ,sumCards
    ,pointTotal
)where



-- draws cards while dealer is under 17, dealer can bust automatically, like in a real game,
-- bets are paid automatically in this case
-- function altered, if total is under 11 and draw 1 (Ace) , count as 11 points, by adding 10 to value to get 21
-- we don't take as under 12 as ace is being added to score as 1 already, we need to add 10 more to it
drawDealerCards :: StdGen -> Int -> [Cards]
drawDealerCards gen total
    |total < 11 && randomNumber == 1 = toEnum (randomNumber - 1) : drawDealerCards newGen (total + randomNumber + 10)
    |total < 17 = toEnum (randomNumber - 1) : drawDealerCards newGen (total + randomNumber)
    |otherwise = []
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

-- draws single card for the player for hit and two at game start
startPlayerCards :: StdGen -> Int -> [Cards]
startPlayerCards gen stop
        |stop /= 0 = toEnum (randomNumber - 1) : startPlayerCards newGen (stop - 1)
        |otherwise = []
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

drawPlayerCard :: StdGen -> [Cards]
drawPlayerCard gen = [toEnum (randomNumber - 1)]
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

-- display value of cards
handValue :: [Cards] -> [Int]
handValue [] = []
handValue (c:cx)
    |fromEnum c >= 10 = fromEnum 10 : handValue cx
    |otherwise = (fromEnum (c) + 1) : handValue cx

-- output of dealers cards to screen for user to see
showCards :: [Cards] -> String
showCards [] = ""
showCards (c:cs)
    |fromEnum c < 10 && fromEnum c > 0  = show (fromEnum (c) + 1) ++ ", " ++ showCards cs
    |otherwise = show c ++ ", " ++ showCards cs


-- display cards enum
handDisplay :: [Cards] -> [Int]
handDisplay [] = []
handDisplay (c:cx) = (fromEnum (c) + 1) : handDisplay cx


-- display card of value chosen
displayCard :: [Int] -> [Cards]
displayCard [] = []
displayCard (x:xs) = toEnum (x - 1) : displayCard xs

-- sum cards altered as if the count of cards is less than 12, there must be an ace in it so add 10 to results of sum
-- only for dealer, player will be different
sumCards :: [Int] -> Int
sumCards cards
    |sum cards < 12 && 1 `elem` cards = sum (cards) + 10
    |otherwise = sum cards


-- show dealer and player cards and total on each draw
-- this runs when user chooses to STAND
pointTotal :: [Cards] -> [Cards] -> String
pointTotal dealerCards playerCards
    |sumCards (handValue dealerCards) == sumCards (handValue playerCards) = "Game Draw"
    |sumCards (handValue dealerCards) > sumCards (handValue playerCards) = "You Lose"
    |sumCards (handValue dealerCards) < sumCards (handValue playerCards) = "You Win"