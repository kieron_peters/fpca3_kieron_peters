import Text.Read
import Data.Char
import System.IO
import qualified Data.Text.IO
import Data.List
import System.Environment
import System.Random
import qualified Data.Map

-- rules of blackjack, all cards 2-10 are worth their respective in points, ace is worth 1 or 11 - players
-- choice, J Q K are worth 10 points dealer will auto-chooses 11 unless the total would be greater than 21, if draw occurs,
-- the bet is not lost or won -- say you draw adding betting value would show this and no alteration to money
-- left is made, take bet away or add for win or lose a dealer must continue drawing cards after his first two
-- cards until he has "17 or greater, then stops" this means the dealer will auto draw until he has 17 or more,
-- dealer loses on 22 or greater this means the player chooses to hit or stand on each card drawn payout is 1.5
-- times their bet if they have 21 and dealer is bust or under 21, if values are the same, even at 21, the game
-- is a draw and no money changes hands. Ace can be worth 1 or 11 based on dealers hand, value is 10 or less to
-- calculate this for dealer, player will decide on their own from input choice


-- create a card type
data Cards = A | Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | J | Q | K deriving (Show, Enum, Bounded, Read)

-- main logic for the whole program to begin a game -- last limitation is to remove parse error for reading balance and
-- the other numerical inputs when user enters some words or something not able to be read as a double
main = do
    putStrLn "Enter Target Balance to Win, (You start with 1/2 this balance, min balance target of 20):\n"
    balance <- getLine 
    if (read balance::Double) > 19
        then menu ((read balance::Double) / 2::Double) (read balance::Double)
        else do putStrLn "Enter in valid balance of at least 20 for your target"
                main
    putStrLn "Game over, would you like to play again? type \"Yes\" or \"No\"\n"
    command <- getLine
    if map toLower command == "yes"
        then do main
        else if map toLower command == "no"
                then putStrLn "Thank you for playing BlackJack, GoodBye..."
                else putStrLn "Did not understand your choice, GoodBye..."

-- main menu of the game runs here
menu :: Double -> Double -> IO ()
menu curBalance tarBalance = do
    --let playerHand = [Ace,Two,Three,Four,Five,Six,Seven,Eight,Nine,Ten,Jack,Queen,King]
    --putStr "Cards Displayed are: "
    --putStrLn (show $ displayCard $ handDisplay playerHand)
    --putStr "Card Values are: "
    --putStrLn (show $ handValue playerHand)
    --putStr "Card hand total is: "
    --putStrLn (show $ sumPlayer $ handValue playerHand)
    putStrLn "Welcome to Blackjack type \"Rules\" or \"Play\" to begin, \"Exit\" to quit current game\n"
    command <- getLine
    if map toLower command == "rules"
        then do handle <- openFile "rules.txt" ReadMode
                contents <- hGetContents handle
                putStrLn contents -- rules of the game read from rules.txt file and printed out onto the page
                menu curBalance tarBalance -- returns to menu after the rules or displayed
        else if map toLower command == "play" 
                then do putStrLn "Game Starting...\n" -- sets up the games starting deal, dealer all set and player with 2 cards
                        dealerGen <- newStdGen
                        playerGen <- newStdGen
                        game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) curBalance tarBalance 0
                else if map toLower command == "exit" 
                        then putStrLn "Exiting Game..."
                        else do putStrLn "Welcome to Blackjack type \"Rules\" or \"Play\" to begin, \"Exit\" to quit game\n"
                                menu curBalance tarBalance



-- begins a game of blackjack
-- print does show conversion for you, need only one function - newStdGen used for true randomness of each call instead of getStdGen
game :: [Cards] -> [Cards] -> Double -> Double -> Double -> IO()
game dealerCards playerCards curBalance tarBalance bet = do
    if curBalance >= tarBalance
        then putStrLn $ "Congratulations, you made " ++ show curBalance ++ " and your target was to reach: " ++ show tarBalance ++ " You won the whole game, well done!!"
        else if bet == 0
                then do putStrLn $ "Current Balance you have to goal target: " ++ show curBalance ++ " / " ++ show tarBalance
                        putStrLn "Enter the amount to bet on the next game:\n"
                        betValue <- getLine
                        if (read betValue::Double) < 1 || (read betValue::Double) > curBalance
                            then do putStrLn "Not a valid bet amount, please enter over 0 and under your balance remaining\n"
                                    game dealerCards playerCards curBalance tarBalance 0
                            else do putStrLn "Bet Accepted!!\n"
                                    game dealerCards playerCards curBalance tarBalance (read betValue::Double)
                else if (sumCards $ handValue dealerCards) > 21
                        then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n" 
                                putStrLn "Dealer Busted, You Win"
                                dealerGen <- newStdGen
                                playerGen <- newStdGen
                                game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) (curBalance + bet) tarBalance 0
                        else if (sumCards $ handValue playerCards) > 21
                                then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                        putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n"
                                        putStrLn "You Busted, You Lose"
                                        if (curBalance - bet) < 1 -- cannot get below 0 as not allowed to bet more than you have
                                            then putStrLn "You Lost all your Money, Game Over"
                                            else do dealerGen <- newStdGen
                                                    playerGen <- newStdGen
                                                    game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) (curBalance - bet) tarBalance 0
                                else if (sumCards $ handValue playerCards) == 21
                                        then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                                putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n"
                                                putStrLn "You Got BlackJack, You Win 1.5 times your bet amount!!"
                                                dealerGen <- newStdGen
                                                playerGen <- newStdGen
                                                game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) (curBalance + (bet / 2) + bet) tarBalance 0 -- you get half your bet plus your bet here
                                        else if (sumCards $ handValue playerCards) == (sumCards $ handValue dealerCards)
                                                then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                                        putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n"
                                                        putStrLn "You Drew the Game, No money won or lost this time!!"
                                                        dealerGen <- newStdGen
                                                        playerGen <- newStdGen
                                                        game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) curBalance tarBalance 0 -- no change to curBalance on a draw
                                                else do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n" -- shows display A 2 3 4 5 6 7 8 9 10 J Q K
                                                        putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n" -- sample for player, player has two cards and adds one if chooses hit
                                                        putStrLn "Do you want to HIT or STAND?\n"
                                                        -- control action for when player hits or stands choices - recurses on hit, calculates points on stand
                                                        command <- getLine
                                                        if map toLower command == "hit"
                                                            then do playerGen <- newStdGen
                                                                    let newCard = drawPlayerCard playerGen
                                                                    let newPlayerCards = playerCards ++ newCard
                                                                    game dealerCards newPlayerCards curBalance tarBalance bet
                                                            else if map toLower command == "stand"
                                                                        then if (pointTotal dealerCards playerCards) == "You Win"
                                                                                then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                                                                        putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n" 
                                                                                        putStrLn "You were higher, You Win"
                                                                                        dealerGen <- newStdGen
                                                                                        playerGen <- newStdGen
                                                                                        game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) (curBalance + bet) tarBalance 0 -- you win the bet amount
                                                                                else if (pointTotal dealerCards playerCards) == "You Lose"
                                                                                        then do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                                                                                putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n"
                                                                                                putStrLn "Dealer was higher, You Lose"
                                                                                                if (curBalance - bet) < 0
                                                                                                    then putStrLn "You Lost all your Money, Game Over"
                                                                                                    else do dealerGen <- newStdGen
                                                                                                            playerGen <- newStdGen
                                                                                                            game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) (curBalance - bet) tarBalance 0 -- you lost your bet
                                                                                        else do putStrLn $ "Dealers Cards: " ++ showCards dealerCards ++ "\n"
                                                                                                putStrLn $ "Your Cards: " ++ showCards playerCards ++ "\n"
                                                                                                putStrLn "You Drew the Game, No money won or lost this time!!"
                                                                                                dealerGen <- newStdGen
                                                                                                playerGen <- newStdGen
                                                                                                game (drawDealerCards dealerGen 0) (startPlayerCards playerGen 2) curBalance tarBalance 0 -- no change to curBalance on a draw
                                                                        else do putStrLn "Please type in HIT or STAND\n"
                                                                                game dealerCards playerCards curBalance tarBalance bet

-- draws cards while dealer is under 17, dealer can bust automatically, like in a real game,
-- bets are paid automatically in this case
-- function altered, if total is under 11 and draw 1 (Ace) , count as 11 points, by adding 10 to value to get 21
-- we don't take as under 12 as ace is being added to score as 1 already, we need to add 10 more to it
drawDealerCards :: StdGen -> Int -> [Cards]
drawDealerCards gen total
    |total < 11 && randomNumber == 1 = toEnum (randomNumber - 1) : drawDealerCards newGen (total + randomNumber + 10)
    |total < 17 = toEnum (randomNumber - 1) : drawDealerCards newGen (total + randomNumber)
    |otherwise = []
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

-- draws single card for the player for hit and two at game start
startPlayerCards :: StdGen -> Int -> [Cards]
startPlayerCards gen stop
        |stop /= 0 = toEnum (randomNumber - 1) : startPlayerCards newGen (stop - 1)
        |otherwise = []
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

drawPlayerCard :: StdGen -> [Cards]
drawPlayerCard gen = [toEnum (randomNumber - 1)]
    where (randomNumber, newGen) = randomR(1,13) gen :: (Int, StdGen)

-- display value of cards
handValue :: [Cards] -> [Int]
handValue [] = []
handValue (c:cx)
    |fromEnum c >= 10 = fromEnum 10 : handValue cx
    |otherwise = (fromEnum (c) + 1) : handValue cx

-- output of dealers cards to screen for user to see
showCards :: [Cards] -> String
showCards [] = ""
showCards (c:cs)
    |fromEnum c < 10 && fromEnum c > 0  = show (fromEnum (c) + 1) ++ ", " ++ showCards cs
    |otherwise = show c ++ ", " ++ showCards cs


-- display cards enum
handDisplay :: [Cards] -> [Int]
handDisplay [] = []
handDisplay (c:cx) = (fromEnum (c) + 1) : handDisplay cx


-- display card of value chosen
displayCard :: [Int] -> [Cards]
displayCard [] = []
displayCard (x:xs) = toEnum (x - 1) : displayCard xs

-- sum cards altered as if the count of cards is less than 12, there must be an ace in it so add 10 to results of sum
-- only for dealer, player will be different
sumCards :: [Int] -> Int
sumCards cards
    |sum cards < 12 && 1 `elem` cards = sum (cards) + 10
    |otherwise = sum cards



-- show dealer and player cards and total on each draw
-- this runs when user chooses to STAND
pointTotal :: [Cards] -> [Cards] -> String
pointTotal dealerCards playerCards
    |sumCards (handValue dealerCards) == sumCards (handValue playerCards) = "Game Draw"
    |sumCards (handValue dealerCards) > sumCards (handValue playerCards) = "You Lose"
    |sumCards (handValue dealerCards) < sumCards (handValue playerCards) = "You Win"

